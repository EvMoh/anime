import Link from 'next/link'

export default function MainPageLayoutHeader() {
	return (
		<div className='width-full p-1 bg-stone-600 text-white bold'>
			<Link href='/'>Аниме-Кун</Link>
		</div>
	)
}
