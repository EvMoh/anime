export default async function Articles() {
	const articles = await getArticles()

	return (
		<main>
			<h1>Статьи</h1>

			{articles.map((article: any) => (
				<article key={article.title}>
					<h2>{article.title}</h2>
					<p>{article.body}</p>
				</article>
			))}
		</main>
	)
}

async function getArticles() {
	const res = await fetch('http://backend-articles:80/articles')

	const data = await res.json()

	return data.articles
}
