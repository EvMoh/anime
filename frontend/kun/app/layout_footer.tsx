import Link from 'next/link'

export default function Footer() {
	const year = getYear()

	return (
		<footer>
			<p>© 2011 &mdash; {year} Все права защищены</p>
			<Link href='https://github.com/EvMoh/anime' target='_blank'>
				GitHub
			</Link>
			<Link href='https://gitlab.com/EvMoh/anime' target='_blank'>
				GitLab
			</Link>
		</footer>
	)
}

function getYear() {
	return new Date().getFullYear().toString()
}
