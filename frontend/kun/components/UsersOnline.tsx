export default function UsersOnline() {
	const usersOnline = getUsersOnline()

	return (
		<div>
			<b>Список пользователей онлайн</b>

			<ul>
				{usersOnline.map(user => (
					<li key={user}>{user}</li>
				))}
			</ul>
		</div>
	)
}

function getUsersOnline() {
	return ['user1', 'user2', 'user3']
}
