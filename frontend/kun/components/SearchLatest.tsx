export default function SearchLatest() {
	const searchLatest = getSearchLatest()

	return (
		<div>
			<span>На сайте искали</span>

			<ul>
				{searchLatest.map(search => (
					<li key={search}>{search}</li>
				))}
			</ul>
		</div>
	)
}

function getSearchLatest() {
	return [
		'лин мин хо',
		'узы этого мира',
		'неудержимая юность',
		'будьте любимы',
		'одаренные',
		'bloodhound',
		'мой партнер',
		'алые',
		'book',
		'of sun',
		'королева слез',
		'ночная фотостудия',
		'кровавый роман',
	]
}
