import Link from 'next/link'

export default function TopMenu() {
	return (
		<ul>
			<li>
				<Link href='/'>Главная</Link>
			</li>
			<li>
				<Link href='/articles'>Статьи</Link>
			</li>
			<li>
				<Link href='/ratings'>Рейтинги</Link>
			</li>
			<li>
				<Link href='/auth/sign-in'>Вход</Link>
			</li>
			<li>
				<Link href='/auth/sign-up'>Регистрация</Link>
			</li>
		</ul>
	)
}
