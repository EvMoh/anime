# Logging what we are doing ...

Install NextJS 14

```sh
npx shadcn-ui@latest init
npx shadcn-ui@latest add button
npx shadcn-ui@latest add input
npx shadcn-ui@latest add breadcrumb
npx shadcn-ui@latest add form
npx shadcn-ui@latest add toast
```

DarkMode Support

```sh
npm install next-themes
```

Zod Validation

```sh
npm install zod
```
