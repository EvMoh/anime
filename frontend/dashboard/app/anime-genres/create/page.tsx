import { InputForm } from './form'

const AnimeGenresCreate = () => {
	return (
		<div>
			<h1>Добавить новый жанр в аниме</h1>

			<p>
				Первый вариант должен быть указан на <b>Русском языке</b>, потому что
				именно он будет выводиться на сайте. Все последующие варианты
				используются в качестве подсказки при поиске жанра.
			</p>

			<InputForm />
		</div>
	)
}

export default AnimeGenresCreate
