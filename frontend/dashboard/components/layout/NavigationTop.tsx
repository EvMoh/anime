'use client'

import Link from 'next/link'
import { usePathname } from 'next/navigation'

const links = [
	{
		title: 'Главная',
		href: '/',
	},
	{
		title: 'Жанры',
		href: '/anime-genres',
	},
	{
		title: 'Жанры - Создать',
		href: '/anime-genres/create',
	},
]

export const NavigationTop = () => {
	const currentUrl = usePathname()

	return (
		<ul>
			{links.map(link => (
				<li key={link.href}>
					<Link
						href={link.href}
						className={link.href === currentUrl ? 'text-red-500' : ''}
					>
						{link.title}
					</Link>
				</li>
			))}
		</ul>
	)
}
