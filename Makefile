# Run a development build
dev:
	cd docker/dev && docker compose up -d && docker-compose watch --no-up

# Show logs for a development build

dev-logs:
	cd docker/dev && docker compose logs -f

# Run a local production build
local:
	cd docker/local && docker compose up -d && docker-compose watch --no-up

# Show logs for a local production build
local-logs:
	cd docker/local && docker compose logs -f

# Push project to github and gitlab repositories.
git:
	git push github
	git push gitlab

postgres:
	docker run -d --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres

postgres-exec:
	docker exec -it postgres psql -U postgres -d postgres