const { Pool } = require('pg')

const pool = new Pool({
	user: 'postgres',
	password: 'postgres',
	host: 'localhost',
	port: 5432,
	database: 'postgres',
})

module.exports = {
	query: (text: string, params: any) => pool.query(text, params),
}
