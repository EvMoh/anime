import dotenv from 'dotenv'
import express, { Express, Request, Response } from 'express'

dotenv.config()

const app: Express = express()
const port = process.env.PORT || 3333

const db = require('./db')

app.get('/', (req: Request, res: Response) => {
	res.send('Hello World')
})

app.get('/', async (req, res) => {
	try {
		const result = await db.query('SELECT * FROM users')
		res.json(result.rows)
	} catch (err) {
		console.error(err)
		res.status(500).send('Internal Server Error')
	}
})

app.get('/create', async (req: Request, res: Response) => {
	try {
		const result = await db.query(
			'CREATE TABLE IF NOT EXISTS test (id VARCHAR(100), name VARCHAR(100))'
		)
	} catch (err) {
		console.error(err)
		res.status(500).send('Internal Server Error')
	}

	try {
		const result = await db.query(
			"INSERT INTO test VALUES ('3f220610-af79-4745-816f-54df105c80f1', 'dropkick-zlogo-duha')"
		)
	} catch (err) {
		console.error(err)
		res.status(500).send('Internal Server Error')
	}

	try {
		const result = await db.query('SELECT * FROM test')
		res.json(result.rows)
	} catch (err) {
		console.error(err)
		res.status(500).send('Internal Server Error')
	}
})

app.listen(port, () => {
	console.log(`[server]: Server is running at http://localhost:${port}`)
})
