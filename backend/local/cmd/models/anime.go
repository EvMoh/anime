package models

type Anime struct {
	Id string `json:"id"`
}

type Animes struct {
	Animes []Anime `json:"animes"`
}