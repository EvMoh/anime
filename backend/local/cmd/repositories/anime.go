package repositories

import (
	"local/cmd/models"
	"local/cmd/storage"
)

func GetAnimes(animes models.Animes) (models.Animes, error) {
  db := storage.GetDB()

  sqlStatement := `SELECT * FROM anime_id`

  rows, err := db.Query(sqlStatement)

  if err != nil {
    return animes, err
  }
	defer rows.Close()

	for rows.Next() {
		anime := models.Anime{}
		err := rows.Scan(&anime.Id)
		// Exit if we get an error
		if err != nil {
			return animes, err
		}
		animes.Animes = append(animes.Animes, anime)
	}

  return animes, nil
}