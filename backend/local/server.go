package main

import (
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"local/cmd/models"
	"local/cmd/repositories"
	"local/cmd/storage"
)

func main() {
	e := echo.New()

	// Root level middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	storage.InitDB()

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.GET("/anime", func(c echo.Context) error {
		animeList, err := repositories.GetAnimes( models.Animes{} )

		if err != nil {
			return err
		}

		
		return c.JSON(http.StatusOK, animeList)
	})

  s := http.Server{
    Addr:        ":80",
    Handler:     e,
    //ReadTimeout: 30 * time.Second, // customize http.Server timeouts
  }
  if err := s.ListenAndServe(); err != http.ErrServerClosed {
    log.Fatal(err)
  }
}