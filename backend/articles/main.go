package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type Ping struct {
	Ping string `json:"ping"`
}

type ArticleType struct {
	Title string `json:"title"`
	Body  string    `json:"body"`
	Likes int `json:"likes"`
}

type ArticlesType struct {
	Articles []ArticleType `json:"articles"`
}

var article *ArticlesType = &ArticlesType{
		Articles: []ArticleType{
			{
				Title: "Article 1",
				Body: "This is article 1",
				Likes: 0,
			},
			{
				Title: "Article 2",
				Body: "This is article 2",
				Likes: 0,
			},
			{
				Title: "Article 3",
				Body: "This is article 3",
				Likes: 0,
			},
	},
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	var ping *Ping = &Ping{
		Ping: "pong",
	}

	switch r.Method {
	case "GET":
					j, _ := json.Marshal(ping)
					w.Write(j)
	}
}

func articlesHandler(w http.ResponseWriter, r *http.Request) {
        switch r.Method {
        case "GET":
                j, _ := json.Marshal(article)
                w.Write(j)
        }
}

type DramaTitleType struct {
	Title string `json:"title"`
	Language int `json:"language"`
}

type DramaType struct {
	Titles []DramaTitleType `json:"titles"`
}

var dramas *DramaType = &DramaType{
		Titles: []DramaTitleType{
				{
					Title: "Королева слез",
					Language: 0,
				},
				{
					Title: "Queen of Tears",
					Language: 1,
				},
				{
					Title: "눈물의 여왕",
					Language: 2,
				},
		},
}

func dramasHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
					j, _ := json.Marshal(dramas)
					w.Write(j)
	}
}

func main() {
        http.HandleFunc("/", homeHandler)
        http.HandleFunc("/articles", articlesHandler)
				http.HandleFunc("/dramas", dramasHandler)

        log.Println("Go!")
        http.ListenAndServe(":80", nil)
}