## Node JS as a Backend

### [Express]()

mkdir js-express && cd js-express && npm init -y

npm i express dotenv

npm i -D typescript @types/express @types/node

npx tsc --init

tsconfig.json
```json
"outDir": "./dist"
```

npm i -D nodemon ts-node

package.json
```json
  "scripts": {
    "build": "npx tsc",
    "start": "node dist/index.js",
    "dev": "nodemon src/index.ts"
  },
```

```bash
npm install -g concurrently
```

nodemon.json
```
{
  "watch": ["src"],
  "ext": "ts",
  "exec": "concurrently \"npx tsc --watch\" \"ts-node src/index.ts\""
}
```

```bash
npm install pg
npm install -D @types/pg
```

### [Fastify]()

#### Install 

```bash

```

### [Nest]()

### [Adonis](https://adonisjs.com/)

```bash
npm init adonisjs@latest js-adonis -- --kit slim --db postgres --install
```

#### Conslusions

Build in ORM is bad